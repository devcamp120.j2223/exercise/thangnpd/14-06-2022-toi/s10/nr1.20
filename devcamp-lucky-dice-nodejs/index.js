const express = require('express');
const app = express();
const port = 8000;
const path = require('path');

//Import middleware
const luckydiceMiddleware = require('./app/middlewares/luckydiceMiddleware')

app.use('/', luckydiceMiddleware);

app.use(express.static(`views/lucky_dice`)); // Use this for show image

//trả ra file giao diện lucky dice casino
app.get('/', (req, res) => {
  console.log(`__dirname: ${__dirname}`);
  res.sendFile(path.join(`${__dirname}/views/lucky_dice/31.30.html`));
})

//Return random number from 1 to 6 
app.get('/random-number', (req, res) => {
  let randomNum = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
  res.json({
    randomNum: randomNum
  });
})

app.listen(port, () => {
  console.log(`NR1.20 app listening on port ${port}`);
})